package ch.inftec.flyway.core;

import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.callback.FlywayCallback;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.util.logging.Log;
import org.flywaydb.core.internal.util.logging.LogFactory;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * <p>
 * Callback implementation for deleting records from a given metadata table, so that "repeatable" scripts are executed with each migration.
 * Integrate this class in flyway with defining property "flyway.callbacks".
 * </p>
 *
 * <p>
 * The following configuration must be
 * <ul>
 *     <li>flyway-extension.repeatable.active => true/false, false is default</li>
 *     <li>flyway-extension.repeatable.table => the metadata table, must be the same as in property "flyway.table"</li>
 * </ul>
 * </p>
 *
 * <p>
 *   as soon as the main flyway configuration is injected in this callback, the "flyway-extension.repeatable.table" is not needed anymore.
 * </p>
 *
 * @author Roger Brechbühl
 */
public class DeleteMetadataCallback implements FlywayCallback {

    private static final Log LOG = LogFactory.getLog(DeleteMetadataCallback.class);

    @Override
    public void beforeClean(Connection connection) {

    }

    @Override
    public void afterClean(Connection connection) {

    }

    @Override
    public void beforeMigrate(Connection connection) {

        String repeatableTable = System.getProperty("flyway-extension.repeatable.table");

        if (repeatableTable == null) {
            LOG.info(String.format("repeatable feature is disabled, as java system property 'flyway-extension.repeatable.table' is not set! Normal migration procedure will go on, also for repeatable scripts"));
            return;
        }

        //the feature may be temporary inactive, e.g. for an hotfix. Just to avoid that all repeatables are executed
        boolean repeatableIsActive = Boolean.parseBoolean(System.getProperty("flyway-extension.repeatable.active", "false"));
        if (!repeatableIsActive) {
            LOG.info(String.format("clearing of repeatable metadata table %s is not active! Normal migration procedure will go on, also for repeatable scripts", repeatableTable));
            return;
        }


        try {
            DbSupport dbSupport = DbSupportFactory.createDbSupport(connection, false);
            if (!dbSupport.getCurrentSchema().getTable(repeatableTable).exists()) {
                LOG.warn(String.format("repeatable feature is active but metadata table %s does not exist? Java system property 'flyway-extension.repeatable.table' must be set same as 'flyway.table'! Normal migration procedure will go on, also for repeatable scripts", repeatableTable));
                return;
            }

            String sql = "DELETE FROM " + repeatableTable;
            connection.createStatement().execute(sql);
            LOG.info(String.format("successfully cleared repeatable metadata table %s! All repeatable scripts will be applied. Disable this feature with setting '-Dflyway-extension.repeatable.active=false'", repeatableTable));
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }

    }

    @Override
    public void afterMigrate(Connection connection) {

    }

    @Override
    public void beforeEachMigrate(Connection connection, MigrationInfo migrationInfo) {

    }

    @Override
    public void afterEachMigrate(Connection connection, MigrationInfo migrationInfo) {

    }

    @Override
    public void beforeValidate(Connection connection) {

    }

    @Override
    public void afterValidate(Connection connection) {

    }

    @Override
    public void beforeBaseline(Connection connection) {

    }

    @Override
    public void afterBaseline(Connection connection) {

    }

    @Override
    public void beforeInit(Connection connection) {

    }

    @Override
    public void afterInit(Connection connection) {

    }

    @Override
    public void beforeRepair(Connection connection) {

    }

    @Override
    public void afterRepair(Connection connection) {

    }

    @Override
    public void beforeInfo(Connection connection) {

    }

    @Override
    public void afterInfo(Connection connection) {

    }
}
