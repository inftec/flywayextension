/**
 * Copyright 2014 by Inftec GmbH, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.inftec.flyway.core;

import org.flywaydb.core.internal.util.logging.Log;
import org.flywaydb.core.internal.util.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * basic test for repeatable script integration
 *
 * @author Roger Brechbühl
 */
public class DeleteMetadataCallbackMigrationTest extends AbstractRepeatableFlywayTest {

    private static final Log LOG = LogFactory.getLog(DeleteMetadataCallbackMigrationTest.class);

    @Before
    public void setup() throws Exception {
        super.init("derby-repeatable.properties");
    }

    @Test
    public void testAdvancedMigrate() {
        System.setProperty("flyway-extension.repeatable.table", "REPEATABLE");
        System.setProperty("flyway-extension.repeatable.active", "true");
        flyway.setCallbacksAsClassNames("ch.inftec.flyway.core.DeleteMetadataCallback");
        int firstMigration = flyway.migrate();
        Assert.assertEquals(2, firstMigration);

        //add another location to simulate a database change
        List<String> additionalLocations = new ArrayList<>(Arrays.asList(flyway.getLocations()));
        additionalLocations.add("filesystem:./target/test-classes/sql-migrations/r2");
        System.out.println(additionalLocations);
        flyway.setLocations(additionalLocations.toArray(new String[0]));

        int secondMigration = flyway.migrate();
        Assert.assertEquals(4, secondMigration);
    }
}
