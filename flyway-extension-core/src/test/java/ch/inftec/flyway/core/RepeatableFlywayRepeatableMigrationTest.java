/**
 * Copyright 2014 by Inftec GmbH, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.inftec.flyway.core;

import org.flywaydb.core.internal.util.logging.Log;
import org.flywaydb.core.internal.util.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;

/**
 *
 *
 * @author Roger Brechbühl
 */
public class RepeatableFlywayRepeatableMigrationTest extends AbstractRepeatableFlywayTest {

    private static final Log LOG = LogFactory.getLog(RepeatableFlywayRepeatableMigrationTest.class);

    @Before
    public void setup() throws Exception {
        super.init("derby.properties");
    }

    @Test
    public void testRepeatableMigrate() {
        flyway.migrate();
    }
}
