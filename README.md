Flyway extensions
=================

This piece of software is based on [Flyway(TM)](http://flywaydb.org), so all the credits go there.
Flyway is a registered trademark of [Boxfuse GmbH](http://boxfuse.com/) and has no relationship with Inftec GmbH, and is not responsible for this extension.

The extension adds two new features:

* executes repeatable scripts
* executes scripts before and/or after the flyway migrate (this is needed for a very special use case which is
not longer discussed here)

The repeatable feature comes in two flavors:

1. repeatable scripts executed along with the patch scripts, one flyway process, one metadata table
2. repeatable and patch scripts executed in separate processes, two metadata tables

IMPORTANT: When integrating 1), be careful in case you have CUSTOM scripts in your existing metadata table. This
 extension will delete those entries!

This extension has only been tested and used with plain SQL scripts, stored on the filesystem. So, no Java migrations and no Spring integration has been tested and is therefore not supported at the time being!
It has been tested on MySQL and used on Oracle databases, all other database systems are untested!

## Repeatable scripts

More documentation can be found here under [https://inftec.atlassian.net/wiki/display/TEC/Flyway](https://inftec.atlassian.net/wiki/display/TEC/Flyway)

### Motivation of repeatable scripts
The main reason is that in some companies huge (business) logic is developed as procedures/packages,
e.g. with PL/SQL when working with Oracle databases. With the default migration concept it would be necessary to copy
 a migration script and add the change to this new script (even when only one line of 1500 LoC is changed). This also
  means that the history of version control is lost for this file.

### Example with two tables
See two table example in module *flyway-extension-example*.

#### Configuration
- patch and repeatable scripts must reside in different folders
- define another flyway configuration, e.g. _conf/flyway\_repeatable.conf_
- configure _flyway.callbacks=ch.inftec.flyway.core.DeleteMetadataCallback_ in _conf/flyway\_repeatable.conf_
- add flyway-extension-core.jar to the lib directory
- System property _flyway-extension.repeatable.table_ must be same as flyway.table in _conf/flyway\_repeatable.conf_
- System property _flyway-extension.repeatable.active_ must be set to true (is false by default). When this property is default, the repeatable scripts are executed like migration scripts. In other words, the re-execution can be avoided, e.g. when a small hotfix is delivered.

#### Migration process
- First the patch/migration scripts are executed
- After the repeatable scripts are executed

#### Pitfalls/Caveats
- The flyway.table definition, and the _flyway-extension.repeatable.table_ system property which is reduntant, actually!

### Example with one table
See one table example in module *flyway-extension-example*.

#### Configuration
- have a different prefix than the other scripts, currently hardcoded to *R*
- have a higher index than the other scripts (e.g. R99\_1\_\_create-procedure.sql, R99\_2\_\_create-view.sql)

#### Migration process
- With a custom migration resolver, all R(epeatable) scripts are added to the pool of migrations.
- With a beforeMigration callback, all rows with CUSTOM type are deleted, which are created by executing repeatable scripts
- During flyway.migrate, first the normal migration scripts (e.g. V1.0\_\_create-table.sql) are executed (once per instance), then all repeatable scripts are (re)executed

#### Pitfalls/Caveats
- In a migration script it's not allowed to reference/use database objects from repeatable scripts
- To drop an object which is created with repeatable scripts, special procedures must be applied (a possible approach will be described later)
- Only commandline client is currently supported

## Before/after scripts
This feature is a very basic implementation, so don't expect too much.
The main purpose of the feature is to execute scripts as a different user but against the same database instance (*flyway.url*).

### Configuration

in flyway.properties add the following properties

* _before.scripts.location_ with the location of the scripts. They are "randomly" (based on the OS) executed!
* _before.user_ the username
* _before.password_ the password

same for scripts which should be executed after the flyway process
* _after.scripts.location_
* _after.user_
* _after.password_

## Versions
### Release 3.2.1-Final
* Integration of [Flyway 3.2.1](http://flywaydb.org/documentation/releaseNotes.html)
* repeatable scripts with two metadata table approach (which is a less invasive implementation)
* Built with Java 1.7

### Release 1.3.1-Final
* Integration of [Flyway 3.1](http://flywaydb.org/documentation/releaseNotes.html)
* Built with Java 1.7

### Release 1.3.0-Final-x
* Integration of [Flyway 3.0](http://flywaydb.org/documentation/releaseNotes.html)
* repeatable scripts with one metadata table
* Built with Java 1.7
