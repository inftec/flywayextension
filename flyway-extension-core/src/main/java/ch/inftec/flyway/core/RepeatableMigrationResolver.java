/**
 * Copyright 2014 by Inftec GmbH, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.inftec.flyway.core;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationType;
import org.flywaydb.core.api.resolver.MigrationResolver;
import org.flywaydb.core.api.resolver.ResolvedMigration;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.resolver.ResolvedMigrationImpl;
import org.flywaydb.core.internal.resolver.sql.SqlMigrationResolver;
import org.flywaydb.core.internal.util.Location;
import org.flywaydb.core.internal.util.PlaceholderReplacer;

import java.sql.SQLException;
import java.util.Collection;

/**
 *
 *
 * @author Roger Brechbühl
 */
public class RepeatableMigrationResolver implements MigrationResolver {

    private Flyway flyway;
    private final String prefix;
    private Location location;
    private DbSupport dbSupport;

    public RepeatableMigrationResolver(Flyway flyway, String prefix, Location location) throws SQLException {
        this.prefix = prefix;
        this.location = location;
        this.flyway = flyway;
        this.dbSupport = DbSupportFactory.createDbSupport(flyway.getDataSource().getConnection(), false);
    }


    @Override
    public Collection<ResolvedMigration> resolveMigrations() {

        SqlMigrationResolver delegate = new SqlMigrationResolver(this.dbSupport, null, getLocation(), getPlaceholderReplacer(), getEncoding(), getSqlMigrationPrefix(), flyway.getSqlMigrationSeparator(), getSqlMigrationSuffix());
        Collection<ResolvedMigration> resolvedMigrations = delegate.resolveMigrations();
        for (ResolvedMigration migration : resolvedMigrations) {
            ((ResolvedMigrationImpl) migration).setType(MigrationType.CUSTOM);
        }
        return resolvedMigrations;

    }

    public String getSqlMigrationSuffix() {
        return flyway.getSqlMigrationSuffix();
    }

    public Location getLocation() {
        return location;
    }

    public String getSqlMigrationPrefix() {
        return prefix;
    }

    public String getEncoding() {
        return flyway.getEncoding();
    }

    public PlaceholderReplacer getPlaceholderReplacer() {
        return new PlaceholderReplacer(flyway.getPlaceholders(), flyway.getPlaceholderPrefix(), flyway.getPlaceholderSuffix());
    }
}
