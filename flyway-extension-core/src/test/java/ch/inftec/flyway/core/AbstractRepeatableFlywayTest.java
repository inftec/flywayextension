/**
 * Copyright 2014 by Inftec GmbH, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.inftec.flyway.core;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.junit.Before;

import java.util.Properties;

/**
 *
 *
 * @author Roger Brechbühl
 */
public class AbstractRepeatableFlywayTest {

    protected final Flyway flyway = new Flyway();
    private RepeatableFlyway repeatableFlyway;

    public void init(String propertyFile) throws Exception {

        Properties properties = new Properties();
        properties.load(RepeatableFlyway.class.getResourceAsStream(propertyFile));
        flyway.configure(properties);

        RepeatableFlyway.configure(flyway, properties);
        DbSupport dbSupport = DbSupportFactory.createDbSupport(flyway.getDataSource().getConnection(), false);
        dbSupport.getCurrentSchema().drop();
        dbSupport.getCurrentSchema().create();
        flyway.baseline();
    }
}
