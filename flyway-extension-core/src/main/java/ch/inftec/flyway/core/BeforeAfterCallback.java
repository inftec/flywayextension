/**
 * Copyright 2014 by Inftec GmbH, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.inftec.flyway.core;

import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.callback.FlywayCallback;
import org.flywaydb.core.internal.util.ClassUtils;
import org.flywaydb.core.internal.util.logging.Log;
import org.flywaydb.core.internal.util.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Class for executing files before and/or after the flyway migration
 *
 * @author Roger Brechbühl
 */
public class BeforeAfterCallback implements FlywayCallback {

    private static final Log LOG = LogFactory.getLog(BeforeAfterCallback.class);
    private Properties properties;

    public BeforeAfterCallback(Properties properties) throws SQLException {
        this.properties = properties;
    }


    @Override
    public void beforeClean(Connection connection) {


    }

    @Override
    public void afterClean(Connection connection) {

    }

    @Override
    public void beforeMigrate(Connection connection) {
        try {

            String beforeLocation = properties.getProperty("before.scripts.location");

            if (beforeLocation != null && beforeLocation.length() > 0) {
                String path = ClassUtils.getLocationOnDisk(BeforeAfterCallback.class);
                path = path.substring(0, path.lastIndexOf("/")) + "/..";

                File baseDir = new File(path + "/" + beforeLocation);

                String password = properties.getProperty("flyway.before.password"); //this would be the configuration from the commandline
                if (password == null || password.length() == 0) {
                    password = properties.getProperty("before.password"); //this would be the configuration from property file
                }
                int filesExecuted = SqlFileRunner.execute(baseDir, properties.getProperty("flyway.url"),
                        properties.getProperty("before.user"), password);
                LOG.info(String.format("executed %d files", filesExecuted));
            }
        } catch (SQLException | IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void afterMigrate(Connection connection) {
        try {
            String afterLocation = properties.getProperty("after.scripts.location");

            if (afterLocation != null && afterLocation.length() > 0) {
                String path = ClassUtils.getLocationOnDisk(BeforeAfterCallback.class);
                path = path.substring(0, path.lastIndexOf("/")) + "/..";

                File baseDir = new File(path + "/" + afterLocation);

                String password = properties.getProperty("flyway.after.password"); //this would be the configuration from the commandline
                if (password == null || password.length() == 0) {
                    password = properties.getProperty("after.password"); //this would be the configuration from property file
                }
                int filesExecuted = SqlFileRunner.execute(baseDir, properties.getProperty("flyway.url"),
                        properties.getProperty("after.user"), password);
                LOG.info(String.format("executed %d files", filesExecuted));
            }
        } catch (SQLException | IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void beforeEachMigrate(Connection connection, MigrationInfo info) {

    }

    @Override
    public void afterEachMigrate(Connection connection, MigrationInfo info) {

    }

    @Override
    public void beforeValidate(Connection connection) {

    }

    @Override
    public void afterValidate(Connection connection) {

    }

    @Override
    public void beforeBaseline(Connection connection) {
        
    }

    @Override
    public void afterBaseline(Connection connection) {

    }

    @Override
    public void beforeInit(Connection connection) {

    }

    @Override
    public void afterInit(Connection connection) {

    }

    @Override
    public void beforeRepair(Connection connection) {

    }

    @Override
    public void afterRepair(Connection connection) {

    }

    @Override
    public void beforeInfo(Connection connection) {

    }

    @Override
    public void afterInfo(Connection connection) {

    }
}
