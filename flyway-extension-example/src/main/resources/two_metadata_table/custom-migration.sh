#!/bin/bash

export _JAVA_OPTIONS="-Dflyway-extension.repeatable.table=haha -Dflyway-extension.repeatable.active=true"
./flyway -configFile=conf/flyway-mysql.properties migrate
./flyway -configFile=conf/flyway-mysql-repeatable.properties migrate
