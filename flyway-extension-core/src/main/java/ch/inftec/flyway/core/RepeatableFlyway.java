/**
 * Copyright 2014 by Inftec GmbH, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.inftec.flyway.core;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.resolver.MigrationResolver;
import org.flywaydb.core.internal.util.Location;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 *
 * @author Roger Brechbühl
 */
public class RepeatableFlyway {

    public static void configure(Flyway flyway, Properties properties) throws SQLException {
        flyway.setCallbacks(new BeforeAfterCallback(properties), new RepeatableCallback(flyway));
        List<MigrationResolver> migrationResolvers = new ArrayList<>();
        for (String locationDescriptor : flyway.getLocations()) {
            migrationResolvers.add(new RepeatableMigrationResolver(flyway, "R", new Location(locationDescriptor)));
        }

        flyway.setResolvers(migrationResolvers.toArray(new MigrationResolver[0]));
    }
}
