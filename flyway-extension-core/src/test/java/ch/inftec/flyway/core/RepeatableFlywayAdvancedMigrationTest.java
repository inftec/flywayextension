/**
 * Copyright 2014 by Inftec GmbH, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.inftec.flyway.core;

import org.flywaydb.core.internal.util.logging.Log;
import org.flywaydb.core.internal.util.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 *
 * @author Roger Brechbühl
 */
public class RepeatableFlywayAdvancedMigrationTest extends AbstractRepeatableFlywayTest {

    private static final Log LOG = LogFactory.getLog(RepeatableFlywayAdvancedMigrationTest.class);

    @Before
    public void setup() throws Exception {
        super.init("derby.properties");
    }

    @Test
    public void testAdvancedMigrate() {
        int firstMigration = flyway.migrate();
        Assert.assertEquals(10, firstMigration);

        List<String> additionalLocations = new ArrayList<>(Arrays.asList(flyway.getLocations()));
        additionalLocations.add("filesystem:./target/test-classes/sql-migrations/patch1");
        flyway.setValidateOnMigrate(false);
        flyway.setLocations(additionalLocations.toArray(new String[0]));

        int secondMigration = flyway.migrate();

        Assert.assertEquals(3, secondMigration);

    }
}
