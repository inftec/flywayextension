How to execute this example
===========================

For repeatable execution use a database like MySQL (and not in-memory like Derby)

    # mysql --user=root -p

    CREATE DATABASE flyway;
    CREATE USER 'flyway'@'localhost' IDENTIFIED BY 'flyway';
    GRANT ALL PRIVILEGES ON *.* TO 'flyway'@'localhost';
    FLUSH PRIVILEGES;

then build the project

    mvn clean install
    cd flyway-extension-example/target

use the generated command line client of your choice

# one metadata table


    # repeatable and migration scripts with one flyway metadata table
    unzip flyway-extension-example-*-one_table.zip -d example1
    cd example1
    ./flyway -configFile=conf/flyway-mysql.properties migrate


# two metadata tables (recommended)


    # repeatable scripts with two flyway metadata tables, one with normal migration scripts, the other with the repeatable scripts
    unzip flyway-extension-example-*-two_tables.zip -d example2
    cd example2; chmod 744 flyway
    export _JAVA_OPTIONS="-Dflyway-extension.repeatable.table=schema_version_r -Dflyway-extension.repeatable.active=true"
    ./flyway -configFile=conf/flyway-mysql.properties migrate
    ./flyway -configFile=conf/flyway-mysql-repeatable.properties migrate


# three metadata tables (pure Flyway)


    # repeatable scripts with three flyway metadata tables, the third table is only used during the deployment
    unzip flyway-extension-example-*-three_tables.zip -d example3
    cd example3; chmod 744 flyway
    ./flyway -configFile=conf/flyway-mysql.properties migrate
    ./flyway -configFile=conf/flyway-mysql-prepare.properties migrate
    ./flyway -configFile=conf/flyway-mysql-repeatable.properties migrate

## Results of running the one table example
### 1st execution

    Flyway 3.2.1 by Boxfuse

    Database: jdbc:mysql://localhost:3306/flyway (MySQL 5.5)
    Validated 4 migrations (execution time 00:00.011s)
    Creating Metadata table: `flyway`.`schema_version`
    Current version of schema `flyway`: << Empty Schema >>
    Migrating schema `flyway` to version 1.1 - create-table-person
    Migrating schema `flyway` to version 1.2 - create-table-address
    Migrating schema `flyway` to version 1.3 - create-table-configuration
    Migrating schema `flyway` to version 99.1 - create-person-address-view
    Successfully applied 4 migrations to schema `flyway` (execution time 00:00.252s).

### 2nd execution

    Flyway 3.2.1 by Boxfuse

    Database: jdbc:mysql://localhost:3306/flyway (MySQL 5.5)
    Validated 4 migrations (execution time 00:00.018s)
    Current version of schema `flyway`: 1.3
    Migrating schema `flyway` to version 99.1 - create-person-address-view
    Successfully applied 1 migration to schema `flyway` (execution time 00:00.039s).

## Results of running the two table example
### 1st execution
#### part 1 - migration scripts

    Flyway 3.2.1 by Boxfuse

    Database: jdbc:mysql://localhost:3306/flyway (MySQL 5.5)
    Validated 3 migrations (execution time 00:00.012s)
    Creating Metadata table: `flyway`.`schema_version`
    Current version of schema `flyway`: << Empty Schema >>
    Migrating schema `flyway` to version 1.1 - create-table-person
    Migrating schema `flyway` to version 1.2 - create-table-address
    Migrating schema `flyway` to version 1.3 - create-table-configuration
    Successfully applied 3 migrations to schema `flyway` (execution time 00:00.209s).

#### part 2 - repeatable scripts
    Picked up _JAVA_OPTIONS: -Dflyway-extension.repeatable.table=schema_version_r -Dflyway-extension.repeatable.active=true
    Flyway 3.2.1 by Boxfuse

    Database: jdbc:mysql://localhost:3306/flyway (MySQL 5.5)
    Validated 1 migration (execution time 00:00.009s)
    Creating Metadata table: `flyway`.`schema_version_r`
    Schema baselined with version: 0
    successfully cleared repeatable metadata table schema_version_r! All repeatable scripts will be applied. Disable this feature with setting '-Dflyway-extension.repeatable.active=false'
    Current version of schema `flyway`: << Empty Schema >>
    Migrating schema `flyway` to version 001 - create-person-address-view
    Successfully applied 1 migration to schema `flyway` (execution time 00:00.032s).

### 2n execution
#### part 1 - migration scripts
    Picked up _JAVA_OPTIONS: -Dflyway-extension.repeatable.table=schema_version_r -Dflyway-extension.repeatable.active=true
    Flyway 3.2.1 by Boxfuse

    Database: jdbc:mysql://localhost:3306/flyway (MySQL 5.5)
    Validated 3 migrations (execution time 00:00.016s)
    Current version of schema `flyway`: 1.3
    Schema `flyway` is up to date. No migration necessary.

#### part 2 - repeatable scripts
    Picked up _JAVA_OPTIONS: -Dflyway-extension.repeatable.table=schema_version_r -Dflyway-extension.repeatable.active=true
    Flyway 3.2.1 by Boxfuse

    Database: jdbc:mysql://localhost:3306/flyway (MySQL 5.5)
    Validated 1 migration (execution time 00:00.012s)
    successfully cleared repeatable metadata table schema_version_r! All repeatable scripts will be applied. Disable this feature with setting '-Dflyway-extension.repeatable.active=false'
    Current version of schema `flyway`: << Empty Schema >>
    Migrating schema `flyway` to version 001 - create-person-address-view
    Successfully applied 1 migration to schema `flyway` (execution time 00:00.034s).

