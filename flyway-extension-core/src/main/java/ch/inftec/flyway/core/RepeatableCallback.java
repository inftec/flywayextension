/**
 * Copyright 2014 by Inftec GmbH, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.inftec.flyway.core;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.MigrationType;
import org.flywaydb.core.api.MigrationVersion;
import org.flywaydb.core.api.callback.FlywayCallback;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.JdbcTemplate;
import org.flywaydb.core.internal.metadatatable.AppliedMigration;
import org.flywaydb.core.internal.util.jdbc.RowMapper;
import org.flywaydb.core.internal.util.logging.Log;
import org.flywaydb.core.internal.util.logging.LogFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Callback implementation for integration of repeatable scripts.
 * The main implemented method is RepeatableCallback#beforeMigrate where all CUSTOM rows are deleted from the
 * metadata table.
 *
 * @author Roger Brechbühl
 *
 */
public class RepeatableCallback implements FlywayCallback {

    private static final Log LOG = LogFactory.getLog(RepeatableCallback.class);
    private final Flyway flyway;
    private List<AppliedMigration> appliedMigrations;

    public RepeatableCallback(Flyway flyway) throws SQLException {
        this.flyway = flyway;
    }


    @Override
    public void beforeClean(Connection connection) {
    }

    @Override
    public void afterClean(Connection connection) {
    }

    /**
     * delete all scripts from metadata table which has been registered as CUSTOM
     *
     * @param connection
     */
    @Override
    public void beforeMigrate(Connection connection) {
        try {
            DbSupport dbSupport = DbSupportFactory.createDbSupport(connection, false);
            if (!dbSupport.getCurrentSchema().getTable(flyway.getTable()).exists()) {
                return;
            }

            String sql = "DELETE FROM " + flyway.getTable()
                    + " WHERE " + dbSupport.quote("type") + "='CUSTOM'";
            connection.createStatement().execute(sql);
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void afterMigrate(Connection connection) {
    }

    @Override
    public void beforeEachMigrate(Connection connection, MigrationInfo info) {

    }

    @Override
    public void afterEachMigrate(Connection connection, MigrationInfo info) {

    }

    @Override
    public void beforeValidate(Connection connection) {


    }

    @Override
    public void afterValidate(Connection connection) {
    }

    @Override
    public void beforeBaseline(Connection connection) {

    }

    @Override
    public void afterBaseline(Connection connection) {

    }

    @Override
    public void beforeInit(Connection connection) {

    }

    @Override
    public void afterInit(Connection connection) {

    }

    @Override
    public void beforeRepair(Connection connection) {

    }

    @Override
    public void afterRepair(Connection connection) {

    }

    /**
     * storing in-memory (!) all CUSTOM entries, and finally removes them from the metadata table. See also
     * method afterInfo.
     *
     * @param connection
     */
    @Override
    public void beforeInfo(Connection connection) {
        try {
            DbSupport dbSupport = DbSupportFactory.createDbSupport(connection, false);
            JdbcTemplate jdbcTemplate = dbSupport.getJdbcTemplate();

            String query = "SELECT " + dbSupport.quote("version_rank")
                    + "," + dbSupport.quote("installed_rank")
                    + "," + dbSupport.quote("version")
                    + "," + dbSupport.quote("description")
                    + "," + dbSupport.quote("type")
                    + "," + dbSupport.quote("script")
                    + "," + dbSupport.quote("checksum")
                    + "," + dbSupport.quote("installed_on")
                    + "," + dbSupport.quote("installed_by")
                    + "," + dbSupport.quote("execution_time")
                    + "," + dbSupport.quote("success")
                    + " FROM " + flyway.getTable();

            query += " WHERE " + dbSupport.quote("type") + " = 'CUSTOM'";
            query += " ORDER BY " + dbSupport.quote("version_rank");

            appliedMigrations = jdbcTemplate.query(query, new RowMapper<AppliedMigration>() {
                public AppliedMigration mapRow(final ResultSet rs) throws SQLException {
                    Integer checksum = rs.getInt("checksum");
                    if (rs.wasNull()) {
                        checksum = null;
                    }

                    return new AppliedMigration(
                            rs.getInt("version_rank"),
                            rs.getInt("installed_rank"),
                            MigrationVersion.fromVersion(rs.getString("version")),
                            rs.getString("description"),
                            MigrationType.valueOf(rs.getString("type")),
                            rs.getString("script"),
                            checksum,
                            rs.getTimestamp("installed_on"),
                            rs.getString("installed_by"),
                            rs.getInt("execution_time"),
                            rs.getBoolean("success")
                    );
                }
            });

            String sql = "DELETE FROM " + flyway.getTable()
                    + " WHERE " + dbSupport.quote("type") + "='CUSTOM'";
            connection.createStatement().execute(sql);
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * re-insert all CUSTOM entries which have been stored in-memory beforeInfo. Note that beforeInfo/afterInfo are
     * not executed in an transactional way, so in case there is an exception between before and after,
     * it might be that the repeatable entries are lost!
     *
     * @param connection
     */
    @Override
    public void afterInfo(Connection connection) {
        try {
            for (AppliedMigration appliedMigration : appliedMigrations) {
                DbSupport dbSupport = DbSupportFactory.createDbSupport(connection, false);
                JdbcTemplate jdbcTemplate = dbSupport.getJdbcTemplate();
                jdbcTemplate.update("INSERT INTO " + flyway.getTable()
                        + " (" + dbSupport.quote("version_rank")
                        + "," + dbSupport.quote("installed_rank")
                        + "," + dbSupport.quote("version")
                        + "," + dbSupport.quote("description")
                        + "," + dbSupport.quote("type")
                        + "," + dbSupport.quote("script")
                        + "," + dbSupport.quote("checksum")
                        + "," + dbSupport.quote("installed_by")
                        + "," + dbSupport.quote("execution_time")
                        + "," + dbSupport.quote("success")
                        + ")"
                        + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                        appliedMigration.getVersionRank(),
                        appliedMigration.getInstalledRank(),
                        appliedMigration.getVersion().getVersion(),
                        appliedMigration.getDescription(),
                        appliedMigration.getType().name(),
                        appliedMigration.getScript(),
                        appliedMigration.getChecksum(),
                        appliedMigration.getInstalledBy(),
                        appliedMigration.getExecutionTime(),
                        appliedMigration.isSuccess());
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
    }


}
