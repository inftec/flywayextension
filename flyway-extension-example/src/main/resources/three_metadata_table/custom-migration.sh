#!/bin/bash

./flyway -configFile=conf/flyway-mysql.properties migrate
./flyway -configFile=conf/flyway-mysql-prepare.properties migrate
./flyway -configFile=conf/flyway-mysql-repeatable.properties migrate
